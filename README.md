# TestPHP Project
   
   ### PHP test proposed by C.E.F.R. in 2019 :
   Goal : create a registration form (Last name - First name - Login - Email - Password) and an authentication form.
   
   Once the user is logged in, he can publish and "like" one or more comments.
   
   ---
   
   ## System requirments

   * PHP 7 or higher;
   * [composer][2]
   * MySQL or MariaDB;
   * and the [usual Symfony 4 application requirements][3].
  
  
   #### Creating database/tables
   Use symfony to create the database :

   ```
   $ php bin/console doctrine:database:create
   $ php bin/console doctrine:migations:diff
   ```

   ##### Loading tables
   Use symfony migration tool :
   ```
   $ php bin/console make:migration
   $ php bin/console doctrine:migrations:migrate
   ```

   #### Loading fixtures using DoctrineFixturesBundle
   Fixtures are a "fake" set of data ready to be loaded into the database.

   Before starting the application, you should load them because they provides a "starting pack display" concerning the main function of the application.

   ```
   $ php bin/console doctrine:fixtures:load
   ```
   

   ##### Launch the project
   Launch and find the application at http://127.0.0.1:8000/ using :
   ```
   $ php bin/console server:run
   ```

   ---
   
   ## My different sources :
   - [Grafikart's Tutorial : Security Component](https://youtu.be/5LfSTeyvyuM)
   - [Symfony 4 Documentation Guide : Security](https://symfony.com/doc/current/security.html)
   - [Symfony 4 Documentation : Security Component](https://symfony.com/doc/current/components/security.html)
   - [Symfony 4 Documentation Guide : Registration Form](https://symfony.com/doc/current/doctrine/registration_form.html)
   - [Symfony 4 Documentation : Validation](https://symfony.com/doc/current/validation.html)
   - [Lior Chamla's Tutorial about Authentication](https://youtu.be/_GjHWa9hQic)
   
   ---
   
   I choose Symfony's Security component to achieve the authentication part using BCrypt instead of FOSUserBundle for example.
   I already know partly how to use the Security component and also because it's provided by default by Symfony.
   
   I also saw that some bundles exists for the "like comment" part but they were not easy to use and didn't provide me what exactly I wanted.
   So I use a simple post method which return me the comment's id to update and it work well.. But the like isn't diplayed in real time..
   
   Lastly, I didnt' use a specific bundle but I learn a lot about Symfony's Security component and Validation, among other things.
   
   PS : I hope you understand that I didn't really use Git's features (3 commits on master) but it isn't no practical for me to use branches when you are alone on a little project.
   
   ---

[2]: https://getcomposer.org/
[3]: https://symfony.com/doc/current/reference/requirements.html
