<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route("/")
     * @Route("/home", name="home")
     * @param Request $request
     * @param CommentRepository $commentRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, CommentRepository $commentRepo)
    {
        /* We want to find all the comments to display them on the homepage : */
        $comments = $commentRepo->findAll();

        $form=null;

        $userCourant = $this->getUser();
        if ($userCourant != null){
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $comment->setAuthor($userCourant);
                try {
                    $comment->setDate(new \DateTime("now"));
                } catch (\Exception $e) {
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();

                $idComm = $comment->getId();
                $urlComment = '/#commentaire_'.$idComm;

                /* User is redirect to his comment */
                /* TODO : Display success info concerning "the good publishment" and smooth scrolling to reach the comment */
                return $this->redirect($urlComment);
            }

            $form = $form->createView();
        }


        return $this->render('base/index.html.twig', [
            'form' => $form,
            'controller_name' => 'BaseController',
            'comments' => $comments
        ]);
    }

    /**
     * @Route("/likeClicked", name="likeClicked")
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likeClicked(Request $request, CommentRepository $commentRepository)
    {
        //Here is the user who liked a comment :
        $userCourant = $this->getUser();

        if($request->isMethod('post')){
            $rep = $request->request->get("like");

            $idComm = intval($rep);
            $commentToUpdate = $commentRepository->find($idComm);

            /* if the user haven't already liked this comment, he is now one of the "likers" */
            if (!$commentToUpdate->getWholikes()->contains($userCourant)){
                $commentToUpdate->addLike($userCourant);
            }else{
                $commentToUpdate->removeLike($userCourant);
            }

            /* Storage of the comment updated */
            $this->getDoctrine()->getManager()->merge($commentToUpdate);
            $this->getDoctrine()->getManager()->flush();


            /* I give up w/ this kind of redirection because it was to messy :
            $urlComment = '/#commentaire_'.$idComm
            //User is redirect to the comment he liked :
            return $this->redirect($urlComment);
            */

            return $this->redirect("home");
        }
    }
}
