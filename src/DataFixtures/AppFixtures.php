<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
     private $passwordEncoder;

     public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
         $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        /* Just 4-5 fixtures to show the main content of the application */

        $user1 = new User();
        $user1->setUsername('Pierre_Dupont')
            ->setFirstname('Pierre')
            ->setLastname('Dupont')
            ->setEmail('pdupont@mail.com');
        $user1->setPassword($this->passwordEncoder->encodePassword(
            $user1,
            'azerty17'
        ));
        $manager->persist($user1);


        $user2 = new User();
        $user2->setUsername('Anto86')
            ->setFirstname('Anto')
            ->setLastname('Depr')
            ->setEmail('adepr@mail.com');
        $user2->setPassword($this->passwordEncoder->encodePassword(
            $user2,
            '123456'
        ));
        $manager->persist($user2);


        $comment1 = new Comment();
        $comment1->setText("Bonjour à tous, je suis nouveau sur le site et je laisse un commentaire !!")
            ->setDate(new \DateTime("now"))
            ->setAuthor($user1)
            ->addLike($user1)
            ->addLike($user2);
        $manager->persist($comment1);


        $comment2 = new Comment();
        $comment2->setText("Salut ! Moi aussi je me suis inscrit et je peux même liker les commentaires !")
            ->setDate(new \DateTime("now"))
            ->setAuthor($user2)
            ->addLike($user1);
        $manager->persist($comment2);


        $comment3 = new Comment();
        $comment3->setText("Ah oui tient, moi aussi !")
            ->setDate(new \DateTime("now"))
            ->setAuthor($user2);
        $manager->persist($comment3);


        $manager->flush();
    }
}
