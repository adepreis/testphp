<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Assert\Length(min="10", maxMessage="Votre message doit contenir au minimum 10 caractères.")
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $wholikes;

    /**
     * @ORM\Column(type="integer")
     */
    private $likenumber;

    public function __construct()
    {
        $this->wholikes = new ArrayCollection();
        $this->likenumber = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getWholikes(): Collection
    {
        return $this->wholikes;
    }

    public function addLike(User $wholike): self
    {
        if (!$this->wholikes->contains($wholike)) {
            $this->wholikes[] = $wholike;
            $this->likenumber += 1;
        }

        return $this;
    }

    public function removeLike(User $whounlike): self
    {
        if ($this->wholikes->contains($whounlike)) {
            $this->wholikes->removeElement($whounlike);
            $this->likenumber -= 1;
        }

        return $this;
    }

    public function getLikenumber(): ?int
    {
        return $this->likenumber;
    }
}
