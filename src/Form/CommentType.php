<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Veuillez renseigner ce champ',
                ]),
                new Length([
                    'min' => 10,
                    'minMessage' => 'Votre commentaire doit faire au moins {{ limit }} caractères',
                ]),
            ],
        ]);
    }
}
